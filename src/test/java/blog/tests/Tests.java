package test.java.blog.tests;

import io.testproject.java.sdk.v2.Runner;

import main.java.blog.tests.BlogSubscribeTest;

public class Tests {
	
	public static void runTest(Runner runner) throws Exception {
		BlogSubscribeTest blogSubscribeTest = new BlogSubscribeTest();

		blogSubscribeTest.email = "suresoft.qa@gmail.com";

		runner.run(blogSubscribeTest);
	}
}