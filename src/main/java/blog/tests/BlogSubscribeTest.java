package main.java.blog.tests;

import io.testproject.java.enums.TakeScreenshotConditionType;
import io.testproject.java.sdk.v2.enums.ExecutionResult;
import io.testproject.java.sdk.v2.exceptions.FailureException;
import io.testproject.java.sdk.v2.support.PageFactory;
import io.testproject.java.sdk.v2.tests.WebTest;
import io.testproject.java.sdk.v2.tests.helpers.WebTestHelper;
import io.testproject.java.sdk.v2.reporters.TestReporter;
import io.testproject.java.sdk.v2.drivers.WebDriver;
import io.testproject.java.annotations.v2.Parameter;

import main.java.blog.pages.BlogPage;

import org.openqa.selenium.JavascriptExecutor;

public class BlogSubscribeTest implements WebTest {

	@Parameter(defaultValue = "suresoft.qa@gmail.com")
	public String email;

	public ExecutionResult execute(WebTestHelper helper) throws FailureException {

		WebDriver webDriver = helper.getDriver();
		TestReporter testReporter = helper.getReporter();

		// given
		// someone visits our website blog page
		webDriver.navigate().to("https://suresoftlimited.com/blog");
		JavascriptExecutor jse = (JavascriptExecutor)webDriver;
		jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");

		// and they see the subscription widget
		BlogPage blogPage = PageFactory.initElements(webDriver, BlogPage.class);
		
		testReporter.step("blog subscription is available", blogPage.blogSubscriptionAvailable()
				, TakeScreenshotConditionType.Always);

		// when
		// they enter suresoft.qa@gmail.com into the subscription widget
		blogPage.enterEmail(email);

		// then
		// they see a message thanking them
		testReporter.step("thankyou message is shown", blogPage.subscriptionThankyouMessageShown()
				, TakeScreenshotConditionType.Always);

		// then
		// and they see a popup saying thanks
		testReporter.step("thankyou popup is shown", blogPage.subscriptionThankyouPopupShown()
				, TakeScreenshotConditionType.Always);

		// then
		// and they are sent an email about their subscription
		// not implemented yet

		return ExecutionResult.PASSED;
	}
}
