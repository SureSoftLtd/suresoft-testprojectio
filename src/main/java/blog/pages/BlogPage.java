package main.java.blog.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class BlogPage {

    @FindBy(id="blog_subscription-3")
    WebElement blogSubscription;

    @FindBy(name="email")
    WebElement emailField;

    @FindBy(xpath="//*[@id=\"subscribe-submit\"]/button")
    WebElement submitButton;

    @FindBy(xpath="//*[@id=\"blog_subscription-3\"]/div")
    WebElement subscriptionThankyouMessage;

    @FindBy(xpath="//*[@id=\"actionbar\"]/ul/li[1]/div/div[2]")
    WebElement subscriptionThankyouPopup;

    public boolean blogSubscriptionAvailable() {
        return blogSubscription.isDisplayed();
    }

    public void enterEmail(String email) {
        emailField.sendKeys(email);
        submitButton.click();
    }

    public boolean subscriptionThankyouPopupShown() {
        return subscriptionThankyouPopup.isDisplayed();
    }

    public boolean subscriptionThankyouMessageShown() {
        return subscriptionThankyouMessage.isDisplayed();
    }

    public boolean subscriptionEmailSent() {
        // todo: not implemented yet. Integration needed with email sender for this to pass.
        return false;
    }
}
