TestProject.io
==========

Java SDK example (only current other language is C#)

you add testproject.io to the pom or gradle build file. This gives you their tool which augments appium and selenium webdriver so you don't have to worry about xcode not running on a windows machine for example, and to simplify things.

BUT - all the examples thus far are using the page object model, and not the less sucky version either. 

How it works:
=========
 - they have WebTest (includes testing mobile browsers), AndroidTest and iosTest interfaces, that you implement in your class.
 - the test entry point is the execute() method.
 - you get the webdriver driver (initialized by testproject agent)
 - They also show a Generic test which doesn't use selenium or UI but I wouldn't use this tool for that. 

the src main folder - contains page objects, and the actual tests as distinct classes (these use the execute() method and return ExecutionResult).

the src test folder - contains the test runner class Tests. This uses testproject.io Runner class and you can instantiate the test and pass data. It exposes methods to run the tests, which are then called in other classes (also housed in the src/test folder) that setup a browser driver first, and call the Tests runner class - which is calling the tests in the main folder. However this groups all the tests in one runner - imagine 200+ tests in here, fuck me!
                             - also contains Configuration class with dev token and device id's - imagine dozens of devices in here, fuck me!

so...
     e.g. a desktop runner class sets up a desktop browser driver, calls the Tests runner class passing in a Runner class instance, that Tests runner class runs the tests living in the main folder. 

They also exemplify Action classes that extend e.g. WebAction. These live in an AddOn folder and are run by an Actions runner class (similar setup to the tests). This looks like an attempt to extract actions from element location to make POM less sucky (?) 

There are multiple smells here:
=====================
1) page object model - sucks - breaks SOLID principles
2) grouping every test in a single runner class (Tests) is a maintenance nightmare
3) grouping those tests in each variant runner (mobile / desktop etc) spreads that nightmare

The mobile ones are different, as they don't need to setup different browsers, since they are testing apps. So, e.g. android, one runner (Tests) that has a setup method. That runs the tests - with some data input. BUT - still grouping tests together.
They also exemplify Action classes that extend e.g. AndroidAction.

Question:
=======
Can this be made to work with the Screenplay pattern? With Serenity-BDD? 
Serenity uses its own runner (a JUnit runner) so probably not Serenity. But Screenplay? Can I replicate the pattern using TestProject?


steps:
=====
1) the agent starts in the taskbar so you won't see anything obvious. Look in the Agents tab of the testproject dashboard. If it is disconnected, right-click the agent in the taskbar and restart it.
2) go to the tests page and add test - you can choose mobile, web or code (they accept jar, dll or zip)
3) you need your dev token/key - go to integrations and click on the sdk you chose, it's in there
4) you need a jar packaging config in your tests pom/gradle to make a jar, to upload into the dashboard
5) then create the job to run it
6) you can then watch the monitor and read the report from the run

you can schedule runs


recording manual tests:
========================
You can record a test, say a web test, and then from the drop-down you can export an excel sheet containing the details
and steps for that test as a script. 
You can also generate code from it, into Java or C#. 


debug
==============
you can start a new test, record it, and then run to certain steps, inspect elements, playback etc... handy


report
=====
you can set email notifications on build results... failure only, or always. works well. nice report.


iOS and Android (real) devices:
================================
To hook and register real devices, the steps vary depending on whether it's an iOS or Android device.

Android:
This is easier. Put the device into dev mode, allow USB-debugging and plug it into the usb port of the machine
hosting the TestProject agent. Authorise the device. You can now start a new test and record it against the Android
phone.

iOS:
This is trickier. Because Apple don't want you to use xCode on a non-OSx machine. So you need to have an Apple 
Developer account, and create a certificate in TestProject, upload it into Apple Dev dashboard, and then create
a new certificate from Apple dev dashboard and upload that into TestProject (this is the handshake effectively).
Then you need to generate a provisioning profile in Apple dev dash (you may need to create an AppID) and upload that
into TestProject. Then hook the iPhone/iPad up to the host machine and start a new test, opening the app you want to
test.

You can generate code from these manually recorded tests, into Java or C#, and use that. For testing against a variety
of devices, realistically you need to have access to a cloud-based device farm like BrowserStack or SauceLabs.


jenkins
=====

initial download on windows failed to create the initialPassword in the secrets folder

tried restart - no
tried uninstall and reinstall - no, initially
then it appeared, weird. then I continued setting it up. 

TestProject.io integration with Jenkins:
===================
first get the api key from testproject, in integrations, api, generate key with scope

then go to jenkins, manage, manage plugins, available

install testproject plugin

go to configure system, find testproject section and paste the api key in and click verbose.

then you can integrate testproject projects into the CI process via jenkins

freestyle or pipeline.
===============
new item > freestyle project

add a build step, run testproject project

the API key is the link, when you open the drop downs, your TP project and agent and so on are in there for you.

source code management- git
add the bitbucket repo, add the creds

then you can trigger the build

poll scm is expensive. 
commit trigger? but need a jar?
you can add build step to update the jar, but that jar still has to be made by the automation-suite.....

added jan's build monitor to the jenkins CI.

